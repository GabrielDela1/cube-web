import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;

function token() {
  return {
      headers: {
          Authorization: "Bearer " + window.localStorage.getItem('token'),
      },
  };
}

// get user resources
async function getUserResources(user_id) {
  let response = await axios.get(BASE_URL + "resources/user/" + user_id, token());
  return response.data;
}

async function getAllResources() {
  let response = await axios.get(BASE_URL + 'resources', {}, token());
  return response;
}

async function getResource(id) {
  let response = await axios.get(BASE_URL + 'resources/' + id, token());
  return response.data;
}

async function getResourceList(ressourceList) {
  let response = await axios.post(BASE_URL + 'resources/list', ressourceList, token());
  return response.data;
}

async function getResourceFavoriteOfUser(user_id) {
  let response = await axios.get(BASE_URL + 'resources/user/' + user_id + '/favorites', {}, token());
  return response.data;
}

async function getFavorite(listFavorites) {
  let listResources = [];
  listFavorites.map(async (e) => {
    let response = await getResource(e);
    listResources.push(response);
  })
  return listResources;
}

async function addFavorite(userId, resourceId) {
  let response = await axios.post(BASE_URL + 'users/' + userId + "/favorites/" + resourceId, token());
  return response.data;
}

async function removeFavorite(userId, resourceId) {
  let response = await axios.delete(BASE_URL + 'users/' + userId + "/favorites/" + resourceId, token());
  return response.data;
}

async function getNbComments(resourceId) {
  let response = await axios.get(BASE_URL + 'resources/comments/count/' + resourceId, token());
  return response.data;
}

async function upload(formData) {
  let response = await axios.post(BASE_URL + "resources/upload", formData, token());
  return response.data;
}

async function postResource(data) {
  let response = await axios.post(BASE_URL + "resources", data, token())
  return response.data;
}

export { getUserResources, getAllResources, getResource, getResourceList, getFavorite, addFavorite, getNbComments, removeFavorite, getResourceFavoriteOfUser, upload, postResource };