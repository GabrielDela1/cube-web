import Admin from '../../layouts/Admin';
import { useEffect, useState } from "react";
import TypeLine from '../../components/TypeLine';
import { getTypes } from '../../utils/admin.type.service';
import Link from "next/link";


export default function Types() {
    let [types, setTypes] = useState([]);

    useEffect(() => {
        getTypes().then(res => {
            setTypes(res);
        });
    }, []);

    return (
        <Admin>
            <div className="flex px-4 text-sm text-left text-gray-500">
                <p className='my-auto'>Gestion des types</p>
                <Link href={`/admin/types/create`}>
                    <button className='ml-auto bg-neutral-800 text-white font-bold text-xs px-4 py-2 rounded'>Ajouter</button>
                </Link>
            </div>
            <table className="mx-2 text-sm px-4 text-left text-gray-500 my-4 ">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Nom
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        types.map((type) => {
                            return (
                                <TypeLine key={type._id} data={type} />
                            )
                        })
                    }
                </tbody>
            </table>
        </Admin>
    )
}