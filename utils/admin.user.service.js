import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;
let prefix = 'admin/users/';

// get header token
function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}

async function getUsers() {
    let res = await axios.get(BASE_URL + prefix, token());
    return res.data;
}

// get user admin
async function getUser(_id) {
    let response = await axios.get(BASE_URL + prefix + _id, token());
    return response.data;
}

// delete user admin
async function removeUser(_id) {
    // promt user to confirm
    if (window.confirm("Voulez-vous vraiment supprimer cet utilisateur ?")) {
        let response = await axios.delete(BASE_URL + prefix + _id, token());

        if (response.status === 200) {
            window.location.href = '/admin/users';
        }
        else {
            alert("Erreur lors de la suppression de l'utilisateur");
        }
        return response.data;
    }
}

async function updateUser(data){
    let response = await axios.patch(BASE_URL + prefix + data._id, data, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/users';
    }
    else {
        alert("Erreur lors de la modification de l'utilisateur");
    }
}

async function createUser(data){
    let response = await axios.post(BASE_URL + prefix, data, token());
    if (response.status === 200) {
        window.location.href = '/admin/users';
    }
    else {
        alert("Erreur lors de la création de l'utilisateur");
    }
}

export { getUsers, getUser, removeUser, updateUser, createUser };