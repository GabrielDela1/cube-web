import Link from "next/link";
import Image from 'next/image';
import { getComment, removeComment } from "../utils/admin.comment.service.js";
import { getUser } from "../utils/admin.user.service.js";
import { getResource } from "../utils/admin.resource.service.js";
import URL from '../utils/url.js';
import { useEffect, useState } from "react";
const BASE_URL = URL;

export default function CommentLine({ data }) {

    let [resource, setResource] = useState(null);
    let [user, setUser] = useState(null);

    useEffect(() => {
        getUser(data.id_user).then(res => {
            setUser(res);
        });

        getResource(data.id_resource).then(res => {
            setResource(res);
        });
    }, []);

    return (
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
            <td className="px-6 py-4">
                {
                    resource ?
                        <Link href={`/admin/resources/${resource._id}`}>
                            <p className="cursor-pointer text-indigo-500 hover:underline hover:text-indigo-800">{resource.title}</p>
                        </Link> : null
                }
            </td>
            <td className="px-6 py-4">
                {
                    user ?
                        <Link href={`/admin/users/${user._id}`}>
                            <p className="cursor-pointer text-indigo-500 hover:underline hover:text-indigo-800">{user.firstname + " " + user.lastname}</p>
                        </Link>
                        :
                        null
                }

            </td>
            <td className="px-6 py-4">
                {data.comment}
            </td>
            <td className="px-6 py-4">
                {data.deleted == true ? "Oui" : "Non"}
            </td>
            <td className="flex flex-col px-6 py-4 text-right">
                <Link href={`/admin/comments/${data._id}`}>
                    <button className="cursor-pointer font-medium text-blue-600 dark:text-blue-500 hover:underline">Éditer</button>
                </Link>
                <button onClick={() => removeComment(data._id)} className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Supprimer</button>
            </td>
        </tr>
    )
}