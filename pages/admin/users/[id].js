import Admin from '../../../layouts/Admin';
import { useEffect, useState } from "react";
import { getUser, updateUser } from '../../../utils/admin.user.service';
import { getStatuses } from '../../../utils/admin.status.service';
import { getRoles } from '../../../utils/admin.role.service';


export default function Users({ id }) {
    let [user, setUser] = useState([]);
    let [statuses, setStatuses] = useState([]);
    let [roles, setRoles] = useState([]);

    let [password, setPassword] = useState('');

    let [status, setStatus] = useState('');
    let [role, setRole] = useState('');
    let [firstname, setFirstname] = useState('');
    let [lastname, setLastname] = useState('');
    let [age, setAge] = useState('');
    let [biography, setBiography] = useState('');

    useEffect(async () => {
        let user = await getUser(id);

        setUser(user);
        setStatus(user.status);
        setRole(user.role);
        setFirstname(user.firstname);
        setLastname(user.lastname);
        setAge(user.age);
        setBiography(user.biography);

        getStatuses().then(res => {
            setStatuses(res);
        });

        getRoles().then(res => {
            setRoles(res);
        });
    }, []);

    const changePassword = (e) => { setPassword(e.target.value); };
    const changeStatus = (e) => { setStatus(e.target.value); }
    const changeRole = (e) => { setRole(e.target.value); }
    const changeFirstname = (e) => { setFirstname(e.target.value); }
    const changeLastname = (e) => { setLastname(e.target.value); }
    const changeAge = (e) => { setAge(e.target.value); }
    const changeBiography = (e) => { setBiography(e.target.value); }

    return (
        <Admin>
            <button className='mr-auto px-4 py-2 bg-neutral-800 font-bol text-xs text-white mx-2 mb-4 rounded' onClick={() => history.back()}>Retour</button>

            <div className="px-2 text-sm text-left text-gray-500">
                Gestion des utilisateurs
            </div>

            <div className='mx-2 bg-white border-solid border-2 border-neutral-300 rounded p-4 my-4'>
                <p className='pb-6'>Informations de l'utilisateur</p>
                <div className="relative z-0 w-full mb-6 group">
                    <input type="email" disabled value={user.email} name="floating_email" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
                    <label htmlFor="floating_email" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Adresse e-mail</label>
                </div>
                <div className="relative z-0 w-full mb-6 group">
                    <input type="password" value={password} onChange={changePassword} name="floating_password" id="floating_password" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                    <label htmlFor="floating_password" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Mot de passe</label>
                </div>

                <div className="relative z-0 w-full mb-4 group">
                    <label htmlFor="statuses" className="block mb-2 text-xs font-sm text-gray-500 dark:text-gray-400">Status</label>
                    <select id="statuses" onChange={changeStatus} className="bg-gray-50 border border-gray-300 text-gray-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {
                            statuses.map(data => {
                                return status == data.name ?
                                    <option value={data.name} selected>{data.name}</option>
                                    :
                                    <option value={data.name}>{data.name}</option>
                            })
                        }
                    </select>
                </div>

                <div className="relative z-0 w-full mb-6 group">
                    <label htmlFor="roles" className="block mb-2 text-xs font-sm text-gray-500 dark:text-gray-400">Rôle</label>
                    <select id="roles" onChange={changeRole} className="bg-gray-50 border border-gray-300 text-gray-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {
                            roles.map(data => {
                                return role == data.name ?
                                    <option value={data.name} selected>{data.name}</option>
                                    :
                                    <option value={data.name}>{data.name}</option>
                            })
                        }
                    </select>
                </div>

                <div className="grid xl:grid-cols-2 xl:gap-6">
                    <div className="relative z-0 w-full mb-6 group">
                        <input type="text" value={firstname} onChange={changeFirstname} name="floating_first_name" id="floating_first_name" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
                        <label htmlFor="floating_first_name" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nom</label>
                    </div>
                    <div className="relative z-0 w-full mb-6 group">
                        <input type="text" value={lastname} onChange={changeLastname} name="floating_last_name" id="floating_last_name" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
                        <label htmlFor="floating_last_name" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Prénom</label>
                    </div>
                </div>
                <div className="grid xl:grid-cols-2 xl:gap-6">
                    <div className="relative z-0 w-full mb-6 group">
                        <input type="number" min="15" max="120" value={age} onChange={changeAge} name="floating_age" id="floating_age" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
                        <label htmlFor="floating_phone" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Âge</label>
                    </div>
                </div>

                <div className="relative z-0 w-full mb-6 group">
                    <textarea className='w-full border-solid border-2 border-neutral-200' value={biography} onChange={changeBiography} />
                </div>

                <button onClick={
                    () => {
                        let data = {};
                        data._id = id;

                        if (firstname != '') data.firstname = firstname;
                        if (lastname != '') data.lastname = lastname;
                        if (age != '') data.age = age;
                        if (biography != '') data.biography = biography;
                        if (status != '') data.status = status;
                        if (role != '') data.role = role;
                        if (password != '') data.password = password;

                        updateUser(data);
                    }
                } type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Éditer</button>
            </div>
        </Admin>
    )
}

export async function getServerSideProps(req) {
    return {
        props:
        {
            id: req.query.id
        }
    }
}