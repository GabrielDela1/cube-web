import Link from "next/link";
import Image from 'next/image';
import { removeResource } from "../utils/admin.resource.service.js";
import { getUser } from "../utils/admin.user.service.js";
import URL from '../utils/url.js';
import { useState, useEffect } from "react";
const BASE_URL = URL;

export default function ResourceLine({ data }) {

    const [user, setUser] = useState({});

    useEffect(() => {
        getUser(data.user_id).then(res => {
            setUser(res);
        });
    }, []);

    return (
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
            <td className="px-6 py-4">
                {data.title}
            </td>
            <td className="px-6 py-4">
                {data.status}
            </td>
            <td className="px-6 py-4">
                <Link href={`/admin/users/` + user._id}>
                    <p className="text-indigo-600 hover:text-indigo-900 hover:underline cursor-pointer">{user ? user.firstname + " " + user.lastname : "Inconnue"}</p>
                </Link>
            </td>
            <td className="px-6 py-4">
                {data.created_at}
            </td>
            <td className="flex flex-col px-6 py-4 text-right">
                <Link href={`/admin/resources/${data._id}`}>
                    <button className="cursor-pointer font-medium text-blue-600 dark:text-blue-500 hover:underline">Éditer</button>
                </Link>
                <button onClick={() => removeResource(data._id)} className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Supprimer</button>
            </td>
        </tr>
    )
}