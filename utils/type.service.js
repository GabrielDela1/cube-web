import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;

function token() {
  return {
      headers: {
          Authorization: "Bearer " + window.localStorage.getItem('token'),
      },
  };
}

// get user resources
async function getAllTypes() {
  let response = await axios.get(BASE_URL + "types/", token());
  return response.data;
}

export { getAllTypes };