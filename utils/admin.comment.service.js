import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;
let prefix = 'admin/comments/';

// get header token
function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}

async function getComments() {
    let res = await axios.get(BASE_URL + prefix, token());
    return res.data;
}

// get user admin
async function getComment(_id) {
    let response = await axios.get(BASE_URL + prefix + _id, token());
    return response.data;
}

// delete user admin
async function removeComment(_id) {
    // promt user to confirm
    if (window.confirm("Voulez-vous vraiment supprimer du commentaire")) {
        let response = await axios.delete(BASE_URL + prefix + _id, token());

        if (response.status === 200) {
            window.location.href = '/admin/comments';
        }
        else {
            alert("Erreur lors de la suppression du commentaire");
        }
        return response.data;
    }
}

async function updateComment(_id, data) {
    let response = await axios.patch(BASE_URL + prefix + _id, data, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/comments';
    }
    else {
        alert("Erreur lors de la modification du commentaire");
    }

    return response.data;
}

export { getComments, getComment, removeComment, updateComment };