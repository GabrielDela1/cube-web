import Link from "next/link";
import Image from 'next/image';
import { removeRole } from "../utils/admin.role.service.js";
import URL from '../utils/url.js';
const BASE_URL = URL;

export default function RoleLine({ data }) {


    return (
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
            <td className="px-6 py-4">
                {data.name}
            </td>
            <td className="flex flex-col px-6 py-4 text-right">
                <Link href={`/admin/roles/${data._id}`}>
                    <button className="cursor-pointer font-medium text-blue-600 dark:text-blue-500 hover:underline">Éditer</button>
                </Link>
                <button onClick={() => removeRole(data._id)} className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Supprimer</button>
            </td>
        </tr>
    )
}