import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;
let prefix = 'admin/types/';

// get header token
function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}

async function getTypes() {
    let res = await axios.get(BASE_URL + prefix, token());
    return res.data;
}

// get user admin
async function getType(_id) {
    let response = await axios.get(BASE_URL + prefix + _id, token());
    return response.data;
}

// delete user admin
async function removeType(_id) {
    // promt user to confirm
    if (window.confirm("Voulez-vous vraiment supprimer le type ?")) {
        let response = await axios.delete(BASE_URL + prefix + _id, token());

        if (response.status === 200) {
            window.location.href = '/admin/types';
        }
        else {
            alert("Erreur lors de la suppression du type");
        }
        return response.data;
    }
}

async function createType(name) {
    let response = await axios.post(BASE_URL + prefix, { name: name }, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/types';
    }
    else {
        alert("Erreur lors de la création du type");
    }

    return response.data;
}

async function updateType(_id, name) {
    let response = await axios.patch(BASE_URL + prefix + _id, { name: name }, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/types';
    }
    else {
        alert("Erreur lors de la modification du type");
    }

    return response.data;
}


export { getTypes, getType, removeType, createType, updateType };