import Image from 'next/image'
import Link from 'next/link';
import Layout from '../layouts/Layout'
import { me, logout } from '../utils/auth.service.js';
import { useEffect, useState } from "react";
import RocketLaunchIcon from '@mui/icons-material/RocketLaunch';
import Card from '../components/Card'
import { getUserResources } from '../utils/resource.service.js';
import { getUserRelations } from '../utils/relation.service.js'


export default function Profile() {
    let [resources, setResources] = useState([]);
    let [user, setUser] = useState(null);
    let [nbRelation, setNbRelation] = useState(0);
    // let user = null;
    // if (typeof window !== 'undefined') {
    //     user = JSON.parse(window.localStorage.getItem("user"));
    // }

    useEffect(async () => {
        me().then(me => {
            getUserRelations(me.data.user.user._id).then((x) => {
                setNbRelation(x.length);
            })
        });

        setUser(JSON.parse(window.localStorage.getItem("user")));
        getUserResources(JSON.parse(window.localStorage.getItem("user")).id).then((x) => {
            console.log(x);
            setResources(x);
        })
        // var response = await axios.get('http://185.171.202.52:5000/api/resources/user/' + user._id,{
        //     headers: {
        //       Authorization: "Bearer " + window.localStorage.getItem('token'),
        //     },
        //   });
        // setResources(response.data)
    }, []);

    const src = user != null ? user.avatar : "/img/background-login.jpg";

    return (
        <Layout>
            <div className="flex flex-col mx-auto max-w-3xl">
                <div className='flex my-6 justify-between'>
                    <div className='flex'>
                        <div>
                            <div className='image-container border-solid border-2 border-purple-cube bg-white rounded-full h-24 w-24'>
                                <Image loader={() => src} src={src} layout="fill" className='rounded-full image h-24 w-24' />
                            </div>
                            {/* <Image width={45}  height={45} src="/img/background-login.jpg" className='rounded-full hover:opacity-70 cursor-pointer' /> */}
                        </div>
                        <div>
                            <div className='w-0 mb-auto mx-2 w-fit'>
                                <div className="flex">
                                    <p className='my-auto px-4 whitespace-nowrap text-xl cursor-pointer'>{user != null ? user.firstname + " " + user.lastname : ''}</p>
                                </div>
                                <p className='px-4 whitespace-nowrap text-gray-500'>{user != null ? user.tag : ''}</p>
                                <Link href={"/relations"}>
                                    <p className='px-4 whitespace-nowrap text-gray-500 cursor-pointer hover:underline'>{nbRelation} relations</p>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className='flex' onClick={() => logout()}>
                        <div className='flex ml-auto w-12 h-12 bg-white rounded-full text-center hover:bg-gray-200 transition cursor-pointer'>
                            <div className='w-full ml-auto my-auto text-center text-xl text-purple-500'>
                                <RocketLaunchIcon className='mb-1' />
                            </div>
                        </div>
                    </div>

                </div>

                {
                    resources ?
                        resources.map((resource, index) => {
                            return (
                                <Card key={index} data={resource} />
                            )
                        }) : null
                }
            </div>
        </Layout>
    )
}