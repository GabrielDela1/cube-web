import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;

function token() {
  return {
      headers: {
          Authorization: "Bearer " + window.localStorage.getItem('token'),
      },
  };
}

// get user resources
async function getUser(user_id) {
  if(user_id != undefined) {
    let response = await axios.get(BASE_URL + "users/" + user_id, token());
    return response.data
  }
  return null;
}

async function getAllUsers() {
  let response = await axios.get(BASE_URL + "users", token());
  return response.data;
}

async function createUser(user) {
  let response = await axios.post(BASE_URL + "users", user);
  return response.data;
}

export { getUser, getAllUsers, createUser };