import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;
let prefix = 'admin/resources/';

// get header token
function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}

async function getResources() {
    let res = await axios.get(BASE_URL + prefix, token());
    return res.data;
}

// get user admin
async function getResource(_id) {
    let response = await axios.get(BASE_URL + prefix + _id, token());
    return response.data;
}

// delete user admin
async function removeResource(_id) {
    // promt user to confirm
    if (window.confirm("Voulez-vous vraiment supprimer la ressource ?")) {
        let response = await axios.delete(BASE_URL + prefix + _id, token());

        if (response.status === 200) {
            window.location.href = '/admin/resources';
        }
        else {
            alert("Erreur lors de la suppression de la ressource");
        }
        return response.data;
    }
}

// async function createResource(name) {
//     let response = await axios.post(BASE_URL + prefix, { name: name }, token());

//     if (response.status === 200) {
//         window.location.href = '/admin/resources';
//     }
//     else {
//         alert("Erreur lors de la création de la ressource");
//     }

//     return response.data;
// }

async function updateResource(_id, data) {
    let response = await axios.patch(BASE_URL + prefix + _id, data, token());

    if (response.status === 200) {
        window.location.href = '/admin/resources';
    }
    else {
        alert("Erreur lors de la modification de la ressource");
    }

    return response.data;
}

async function upload(formData) {
    let response = await axios.post(BASE_URL + "resources/upload", formData, token());
    return response.data;
}

export { getResources, getResource, removeResource, updateResource, upload };