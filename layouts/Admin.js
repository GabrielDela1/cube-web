import Head from 'next/head';
import Link from 'next/link';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import { useEffect } from 'react';
import { me, isAdmin } from '../utils/auth.service.js';

export default function Layout({ children }) {

    useEffect(() => {
        let token = window.localStorage.getItem('token');

        me(token);
        isAdmin(token);
    }, []);

    return (
        <div>
            <Head>
                <title>Admin - CUBE</title>
                <meta name="description" content="CUBE administration panel" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className={"flex flex-col bg-gray-100"}>
                <div className={"fixed z-10 top-0 left-0 flex h-14 bg-neutral-800 w-full text-gray-100"}>
                    <Link href="/admin/users">
                        <div className="cursor-pointer text-sm font-bold tracking-widest hover:text-gray-200 transition px-4 my-auto hover:underline">
                            Utilisateurs
                        </div>
                    </Link>
                    <Link href="/admin/resources">
                        <div className="cursor-pointer text-sm font-bold tracking-widest hover:text-gray-200 transition px-4 my-auto hover:underline">
                            Ressources
                        </div>
                    </Link>
                    <Link href="/admin/comments">
                        <div className="cursor-pointer text-sm font-bold tracking-widest hover:text-gray-200 transition px-4 my-auto hover:underline">
                            Comments
                        </div>
                    </Link>
                    <Link href="/admin/roles">
                        <div className="cursor-pointer text-sm font-bold tracking-widest hover:text-gray-200 transition px-4 my-auto hover:underline">
                            Rôles
                        </div>
                    </Link>
                    <Link href="/admin/status">
                        <div className="cursor-pointer text-sm font-bold tracking-widest hover:text-gray-200 transition px-4 my-auto hover:underline">
                            Statuts
                        </div>
                    </Link>
                    <Link href="/admin/categories">
                        <div className="cursor-pointer text-sm font-bold tracking-widest hover:text-gray-200 transition px-4 my-auto hover:underline">
                            Catégories
                        </div>
                    </Link>
                    <Link href="/admin/types">
                        <div className="cursor-pointer text-sm font-bold tracking-widest hover:text-gray-200 transition px-4 my-auto hover:underline">
                            Types
                        </div>
                    </Link>
                    {/* <Link href="">
                        <div className="cursor-pointer font-thin tet-xl tracking-widest hover:text-gray-200 transition px-12 my-auto">
                            Commentaires
                        </div>
                    </Link>
                    <Link href="">
                        <div className="cursor-pointer font-thin tet-xl tracking-widest hover:text-gray-200 transition px-12 my-auto">
                            Ressources
                        </div>
                    </Link> */}
                </div>
                <main style={{ minHeight: '100vh' }} className="pt-20 px-4 flex flex-col mx-auto w-full lg:w-2/3 py-6">
                    {children}
                </main>
            </div>
        </div>
    )
}
