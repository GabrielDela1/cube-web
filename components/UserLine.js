import Link from "next/link";
import Image from 'next/image';
import URL from '../utils/url.js';

import { removeUser } from "../utils/admin.user.service.js";

const BASE_URL = URL;

export default function UserLine({ data }) {

    const myLoader = ({ src }) => {
        return BASE_URL + 'images/' + data.avatar;
    }

    return (
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
            <th scope="row" className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                {
                    data.avatar != null ?
                        <Image alt={data.avatar} unoptimized={true} loader={myLoader} src={data.avatar} width={'50px'} height={'50px'} className='rounded-lg w-10 h-10' />
                        :
                        null
                }
            </th>
            <td className="px-6 py-4">
                {data.tag}
            </td>
            <td className="px-6 py-4">
                {data.firstname + " " + data.lastname}
            </td>
            <td className="px-6 py-4">
                {data.role.charAt(0).toUpperCase() + data.role.slice(1)}
            </td>
            <td className="px-6 py-4">
                {data.status.charAt(0).toUpperCase() + data.status.slice(1)}
            </td>
            <td className="flex flex-col px-6 py-4 text-right">
                <Link href={`/admin/users/${data._id}`}>
                    <button className="cursor-pointer font-medium text-blue-600 dark:text-blue-500 hover:underline">Éditer</button>
                </Link>
                <button onClick={() => removeUser(data._id)} className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Supprimer</button>
            </td>
        </tr>
    )
}