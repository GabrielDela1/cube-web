import Admin from '../../layouts/Admin';
import { useEffect, useState } from "react";
import StatusLine from '../../components/StatusLine';
import { getStatuses } from '../../utils/admin.status.service';
import Link from "next/link";


export default function Status() {
    let [statuses, setStatus] = useState([]);

    useEffect(() => {
        getStatuses().then(res => {
            setStatus(res);
        });
    }, []);

    return (
        <Admin>
            <div className="flex px-4 text-sm text-left text-gray-500">
                <p className='my-auto'>Gestion des statuts</p>
                <Link href={`/admin/status/create`}>
                    <button className='ml-auto bg-neutral-800 text-white font-bold text-xs px-4 py-2 rounded'>Ajouter</button>
                </Link>
            </div>
            <table className="mx-2 text-sm px-4 text-left text-gray-500 my-4 ">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Nom
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        statuses.map((status) => {
                            return (
                                <StatusLine key={status._id} data={status} />
                            )
                        })
                    }
                </tbody>
            </table>
        </Admin>
    )
}