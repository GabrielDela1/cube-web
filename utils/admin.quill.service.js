import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;

async function uploadImage(file) {
    var formData = new FormData();
    formData.append("image", file);

    let response = await axios.post(BASE_URL + "resources/upload", formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        }
    });

    return response.data.image;
}

export { uploadImage };