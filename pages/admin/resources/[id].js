import Admin from '../../../layouts/Admin';
import { useEffect, useState } from "react";
import { getResource, updateResource, upload } from '../../../utils/admin.resource.service';
import { uploadImage } from '../../../utils/admin.quill.service';
import { useQuill } from "react-quilljs";
import { getCategories } from '../../../utils/admin.category.service';
import { getStatuses } from '../../../utils/admin.status.service';
import { getTypes } from '../../../utils/admin.type.service';
import "quill/dist/quill.snow.css";

import URL from '../../../utils/url.js';
const BASE_URL = URL;

export default function Resources({ id }) {
    let [statuses, setStatuses] = useState([]);
    let [categories, setCategories] = useState([]);
    let [types, setTypes] = useState([]);

    let [resource, setResource] = useState({});
    let [title, setTitle] = useState('');
    let [description, setDescription] = useState('');
    let [image, setImage] = useState('');
    let [formData, setFormData] = useState('');
    let [user_id, setUserId] = useState('');
    let [category_id, setCategoryId] = useState('');
    let [type_id, setTypeId] = useState('');
    let [created_at, setCreatedAt] = useState('');
    let [updated_at, setUpdatedAt] = useState('');
    let [status, setStatus] = useState('');

    const { quill, quillRef } = useQuill();

    const isQuillEmpty = (quill) => {
        if ((quill.getContents()['ops'] || []).length !== 1) {
            return false
        }

        return quill.getText().trim().length === 0
    }

    const sanitize = (url, protocols) => {
        var anchor = document.createElement('a');
        anchor.href = url;

        var protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
        return protocols.indexOf(protocol) > -1;
    }

    // do upload
    const uploadToServer = async (file) => {
        let src = uploadImage(file);
        insertToEditor(BASE_URL + "images/" + src);
    }

    const imageHandler = () => {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.click();

        input.onchange = () => {
            const file = input.files[0];
            uploadToServer(file);
        };
    };

    const insertToEditor = (url) => {
        const range = quill.getSelection();
        quill.insertEmbed(range.index, 'image', url);
    };

    const save = async (quill, title, description, formData, category, type) => {

        if (isQuillEmpty(quill)) {
            alert('Vous ne pouvez pas poster une resource vide !');
            return false;
        }

        var html = quill.root.innerHTML;

        if (title !== "" && description !== "" && html !== "" && html != "<p><br></p>" && category !== "" && type !== "") {
            document.body.style.cursor = 'wait';

            let imageURL;
            if(formData){
                imageURL = await upload(formData);
            }

            let data = {
                title: title,
                description: description,
                content: html,
                category_id: category_id,
                type_id: type_id
            };

            if (imageURL) {
                data.image = imageURL.image;
            }

            let response = await updateResource(resource._id, data);

            document.body.style.cursor = 'default';
        }
        else {
            alert("Veuillez remplir tous les champs");
        }
    }

    const prepareDownload = (file, setFormData) => new Promise((resolve, reject) => {
        var formData = new FormData();
        formData.append("image", file);
        setFormData(formData);
    });

    useEffect(() => {
        getCategories().then((x) => {
            setCategories(x);
        });

        getTypes().then((x) => {
            setTypes(x);
        });

        getStatuses().then((x) => {
            setStatuses(x);
        });

        getResource(id).then(res => {
            setResource(res);

            if (quill) {
                quill.getModule('toolbar').addHandler('image', imageHandler);

                quill.clipboard.dangerouslyPasteHTML(res.content);
            }

            setTitle(res.title);
            setDescription(res.description);
            setImage(res.image);
            setUserId(res.user_id);
            setCategoryId(res.category_id);
            setTypeId(res.type_id);
            setCreatedAt(res.created_at);
            setUpdatedAt(res.updated_at);
            setStatus(res.status);
        });
    }, [quill]);

    const changeTitle = (e) => {
        setName(e.target.value);
    }
    const changeDescription = (e) => {
        setDescription(e.target.value);
    }
    const changeCategoryId = (e) => {
        setCategoryId(e.target.value);
    }
    const changeTypeId = (e) => {
        setTypeId(e.target.value);
    }
    const changeStatus = (e) => {
        setStatus(e.target.value);
    }

    const myLoader = ({ src }) => {
        return BASE_URL + 'images/' + image;
    }

    return (
        <Admin>
            <button className='mr-auto px-4 py-2 bg-neutral-800 font-bol text-xs text-white mx-2 mb-4 rounded' onClick={() => history.back()}>Retour</button>

            <div className="px-2 text-sm text-left text-gray-500">
                Gestion des ressources
            </div>

            <div className='mx-2 bg-white border-solid border-2 border-neutral-300 rounded p-4 my-4'>
                <p className='pb-6'>Informations de la ressource</p>
                <div className="relative z-0 w-full mb-6 group">
                    <input type="text" value={title} onChange={changeTitle} name="floating_title" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
                    <label htmlFor="floating_title" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Titre</label>
                </div>

                <div className="relative z-0 w-full mb-6 group">
                    <input type="text" value={description} onChange={changeDescription} name="floating_description" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
                    <label htmlFor="floating_description" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Description</label>
                </div>

                <div className="relative z-0 w-full mb-6 group">
                    <p className="mb-2 text-sm text-gray-500 dark:text-gray-400 ">Description</p>
                    <input id="floating_image" className='mb-6' onChange={async () => {
                        let file = document.getElementById("image").files[0];
                        await prepareDownload(file, setFormData);
                    }} type="file" accept="image/png, image/gif, image/jpeg" />

                    {image ? <img loader={() => BASE_URL + "images/" + image} src={BASE_URL + "images/" + image} /> : null}
                </div>

                <div id="editor" className="bg-gray-100" ref={quillRef} />

                <div className="relative z-0 w-full my-4 group">
                    <label htmlFor="statuses" className="block mb-2 text-xs font-sm text-gray-500 dark:text-gray-400">Status</label>
                    <select id="statuses" onChange={changeStatus} className="bg-gray-50 border border-gray-300 text-gray-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {
                            statuses.map(data => {
                                return status == data.name ?
                                    <option key={data._id} value={data.name} selected>{data.name}</option>
                                    :
                                    <option key={data._id} value={data.name} >{data.name}</option>
                            })
                        }
                    </select>
                </div>

                <div className="relative z-0 w-full mb-4 group">
                    <label htmlFor="categories" className="block mb-2 text-xs font-sm text-gray-500 dark:text-gray-400">Categorie</label>
                    <select id="categories" onChange={changeCategoryId} className="bg-gray-50 border border-gray-300 text-gray-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {
                            categories.map(data => {
                                return status == data.name ?
                                    <option key={data._id} value={data._id} selected>{data.name}</option>
                                    :
                                    <option key={data._id} value={data._id}>{data.name}</option>
                            })
                        }
                    </select>
                </div>

                <div className="relative z-0 w-full mb-4 group">
                    <label htmlFor="types" className="block mb-2 text-xs font-sm text-gray-500 dark:text-gray-400">Type</label>
                    <select id="types" onChange={changeTypeId} className="bg-gray-50 border border-gray-300 text-gray-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {
                            types.map(data => {
                                return status == data.name ?
                                    <option key={data._id} value={data._id} selected>{data.name}</option>
                                    :
                                    <option key={data._id} value={data._id}>{data.name}</option>
                            })
                        }
                    </select>
                </div>

                <div className="relative z-0 w-full mb-6 group">
                    Créer le {new Date(created_at).toLocaleDateString()}, dernièrement modifié le {new Date(updated_at).toLocaleDateString()}
                </div>

                <button onClick={() => {
                    save(quill, title, description, formData, category_id, type_id);
                }
                } type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Éditer</button>
            </div>
        </Admin>
    )
}

export async function getServerSideProps(req) {
    return {
        props:
        {
            id: req.query.id
        }
    }
}