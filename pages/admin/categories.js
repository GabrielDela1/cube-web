import Admin from '../../layouts/Admin';
import { useEffect, useState } from "react";
import CategoryLine from '../../components/CategoryLine';
import { getCategories } from '../../utils/admin.category.service';
import Link from "next/link";


export default function Categories() {
    let [categories, setCategories] = useState([]);

    useEffect(() => {
        getCategories().then(res => {
            setCategories(res);
        });
    }, []);

    return (
        <Admin>
            <div className="flex px-4 text-sm text-left text-gray-500">
                <p className='my-auto'>Gestion des catégories</p>
                <Link href={`/admin/categories/create`}>
                    <button className='ml-auto bg-neutral-800 text-white font-bold text-xs px-4 py-2 rounded'>Ajouter</button>
                </Link>
            </div>
            <table className="mx-2 text-sm px-4 text-left text-gray-500 my-4 ">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Nom
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        categories.map((category) => {
                            return (
                                <CategoryLine key={category._id} data={category} />
                            )
                        })
                    }
                </tbody>
            </table>
        </Admin>
    )
}