import Link from 'next/link';
import PersonRemoveIcon from '@mui/icons-material/PersonRemove';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { RemoveRelation, AddRelations } from '../utils/relation.service'

export default function Relation({ dataUser, isFriend}) {
    let [user, setUser] = useState(null);
    let [relation, setRelation] = useState(null);


    console.log("mon ami",isFriend);
    console.log(dataUser);

    let [myUser, setMyUser] = useState({});





    useEffect(() => {

        setMyUser(JSON.parse(window.localStorage.getItem("user")));
        console.log(dataUser)
        if(isFriend){
            setUser(dataUser.user);
            setRelation(dataUser);
        }else{
            setUser(dataUser);
        }
    }, [dataUser]);

    const RemoveFriend = () => {
        console.log("remove friend ", dataUser)
        RemoveRelation(relation._id).then(() => {
            window.location = window.location;
        })
    }

    const addUser = () => {
        AddRelations({
            id_from : myUser.id,
            id_to : user.id
        }).then(() => {
            window.location = window.location;
        })
    }

    const src = user != null ? (user.avatar != null ? user.avatar : '/img/background-login.jpg') : '/img/background-login.jpg';

    return (
        <div className='px-4 mb-4 flex justify-between bg-white py-4 rounded-full'>
            <div className='flex w-full'>
                <div className='flex rounded-full border-solid border-2 border-purple-cube'>
                    <Image alt={user != null ? user.firstname + " " + user.lastname : "####"} unoptimized={true} loader={() => src} src={src} width='50px' height='50px' className='rounded-full' />
                </div>

                <div className='px-4 tracking-widest font-semibold'>
                <Link href={user != null ? "/profile/" + user.id : "/profile"}>
                    <p className='cursor-pointer hover:underline'>{user != null ? user.firstname + " " + user.lastname : "####"}</p>
                </Link>
                    <div className='text-md font-normal'>{dataUser != null ? dataUser.relation : "######"}</div>
                </div>
                {
                    relation != null 
                    ? 
                    <button className='my-auto h-fit w-14 text-sm ml-auto bg-gray-100 rounded-full p-4 hover:bg-gray-200 cursor-pointer transition' onClick={RemoveFriend}>
                        <PersonRemoveIcon></PersonRemoveIcon>
                    </button>
                    :
                    <button className='my-auto h-fit w-14 text-sm ml-auto bg-gray-100 rounded-full p-4 hover:bg-gray-200 cursor-pointer transition' onClick={addUser}>
                        <i className="fa fa-user-plus" aria-hidden="true"></i>
                    </button>
                }
            </div>
        </div>
    )
}
