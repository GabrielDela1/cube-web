import { Router } from "next/router";
import react from "react"
import { useEffect, useState } from "react";
import { createUser } from "../utils/user.service";

export default function Register() {

  const [lastName, setLastName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [age, setAge] = useState(0);
  const [checkbox, setCheckbox] = useState(false);

  const changeLastName = (e) => {
    setLastName(e.target.value);
  }
  const changeFirstName = (e) => {
    setFirstName(e.target.value);
  }
  const changeEmail = (e) => {
    setEmail(e.target.value);
  }
  const changePassword = (e) => {
    setPassword(e.target.value);
  }
  const changePasswordConfirm = (e) => {
    setPasswordConfirm(e.target.value);
  }
  const changeAge = (e) => {
    setAge(e.target.value);
  }
  const changeCheckbox = (e) => {
    setCheckbox(e.target.checked);
  }

  useEffect(async () => {
    // const token = window.localStorage.getItem("token");
    // await me(token);
  }, []);

  const Inscription = () => {

    console.log(lastName, firstName, email, password, passwordConfirm, age, checkbox);
    if (password !== passwordConfirm) {
      alert("Les mots de passe ne correspondent pas");
    }
    else if (age < 14) {
      alert("Vous devez avoir 14 ans ou plus");
    }
    else if (lastName === "" || firstName === "" || email === "" || password === "" || passwordConfirm === "" || age === 0) {
      alert("Veuillez remplir tous les champs");
    }
    else if (checkbox === false) {
      alert("Veuillez accepter les conditions d'utilisation");
    }
    else if (validateEmail(email) === false) {
      alert("Veuillez entrer une adresse email valide");
    }
    else {
      createUser({
        lastname: lastName,
        firstname: firstName,
        biography: "",
        email: email,
        password: password,
        age: age
      }).then((res) => {
        console.log(res);
        windows.location = "aircube.ga/login";
      })
    }
  }

  //function email validate
  const validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  return (
    <div className={"flex h-full w-full"}>
      <div className="flex flex-col bg-purple-cube h-screen w-full md:w-1/2 lg:w-1/3 justify-center px-10">
        <div>
          <p className={"text-white text-4xl text-center my-4"}>Bienvenue chez AirCube</p>
        </div>
        <hr className={"w-auto mb-5"} />
        <div className="flex flex-col my-5 ">
          <input className="py-6 mb-4 px-2 text-white h-10 bg-transparent rounded-md border border-2 border-gray-200" value={lastName} onChange={changeLastName} type={"text"} placeholder="Nom" />
          <input className="py-6 mb-4 px-2 text-white h-10 bg-transparent rounded-md border border-2 border-gray-200" value={firstName} onChange={changeFirstName} type={"text"} placeholder="Prenom" />
          <input className="py-6 mb-4 px-2 text-white h-10 bg-transparent rounded-md border border-2 border-gray-200" value={email} onChange={changeEmail} type={"text"} placeholder="Email" />
          <input className="py-6 mb-4 px-2 text-white h-10 bg-transparent rounded-md border border-2 border-gray-200" value={password} onChange={changePassword} type={"password"} placeholder="Mot de passe" />
          <input className="py-6 mb-4 px-2 text-white h-10 bg-transparent rounded-md border border-2 border-gray-200" value={passwordConfirm} onChange={changePasswordConfirm} type={"password"} placeholder="Répéter le mot de passe" />
          <input className="py-6 mb-4 px-2 text-white h-10 bg-transparent rounded-md border border-2 border-gray-200" value={age} onChange={changeAge} type={"number"} placeholder="Age" max={120} min={0} />
          <label className="text-white ml-4 w-fit">
            <input type="checkbox" className="mr-5" value={checkbox} onChange={changeCheckbox} />
            {"J'accepte les termes du contrat d'utilisation."}
          </label>
          {/* <Link href={"/"}> */}
          <div className=" py-3 my-4 bg-white font-bold rounded-full hover:bg-gray-100 cursor-pointer transition">
            <button className={"m-auto text-center w-full bold text-xl"} onClick={Inscription}>Inscription</button>
          </div>
          {/* </Link> */}

        </div>
      </div>
      <img src="../img/background-login.jpg" className="w-0 md:w-1/2 lg:w-2/3 object-cover" />
    </div>
  );
}
