import Admin from '../../layouts/Admin';
import { useEffect, useState } from "react";
import CommentLine from '../../components/CommentLine';
import { getComments } from '../../utils/admin.comment.service';
import Link from "next/link";


export default function Comments() {
    let [comments, setComments] = useState([]);

    useEffect(() => {
        getComments().then(res => {
            setComments(res);
        });
    }, []);

    return (
        <Admin>
            <div className="flex px-4 text-sm text-left text-gray-500">
                <p className='my-auto'>Gestion des ressources</p>
            </div>

            <table className="mx-2 text-sm px-4 text-left text-gray-500 my-4 ">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Ressource
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Utilisateur
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Commentaire
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Supprimé ?
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        comments.map((comment) => {
                            return (
                                <CommentLine key={comment._id} data={comment} />
                            )
                        })
                    }
                </tbody>
            </table>
        </Admin>
    )
}