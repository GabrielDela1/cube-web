import Link from "next/link";
import Image from 'next/image';
import { getUser } from "../utils/admin.user.service.js";
import URL from '../utils/url.js';
import { useEffect, useState } from "react";
const BASE_URL = URL;

export default function Comment({ data }) {
    let [user, setUser] = useState(null);

    useEffect(() => {
        getUser(data.id_user).then(res => {
            setUser(res);
        });
    }, []);

    return (
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
            <td className="px-6 py-4 whitespace-no-wrap border-b dark:border-gray-700">
                <div className="flex items-center">
                    <div className="flex-shrink-0 h-10 w-10 rounded-full border-solid border-2 border-purple-cube">
                        {
                            user != null ?
                                <Image loader={() => user.avatar} src={user.avatar} alt={user.avatar} unoptimized={true} width={"45px"} height={"45px"} className="rounded-full h-10 w-10" />
                                :
                                null
                        }

                    </div>
                    <div className="ml-4">
                        <div className="text-sm leading-5 font-medium text-gray-900">{user ? user.firstname + " " + user.lastname : null}</div>

                        <div className="py-4 text-sm leading-5 text-gray-900">{data ? data.comment : null}</div>

                        <div className="text-sm leading-5 text-gray-500">Le {data ? new Date(data.create_date).toLocaleDateString() : null}</div>
                    </div>
                </div>
            </td>
        </tr>
    )
}