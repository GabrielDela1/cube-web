import Image from 'next/image'
import Layout from '../../layouts/Layout'
import { me } from '../../utils/auth.service.js';
import { getUserResources } from '../../utils/resource.service.js';
import { getUser } from '../../utils/user.service.js';
import { useEffect, useState } from "react";
import Card from '../../components/Card';
import { getUserRelations, AddRelations, RemoveRelation } from '../../utils/relation.service.js';
import PersonRemoveIcon from '@mui/icons-material/PersonRemove';

export default function Profile({ userId }) {

    let [resources, setResources] = useState([]);
    let [userData, setUserData] = useState(null);
    let [nbRelation, setNbRelation] = useState(0);
    let [myUser, setMyUser] = useState({});
    let [isMyFriend, setIsMyFriend] = useState(false);
    let [myRelation, setMyRelation] = useState({})

    //a faire dans un getServerSideProps
    useEffect(async () => {
        me();
        let user = JSON.parse(window.localStorage.getItem("user"))
        setMyUser(user);
        if (typeof window !== 'undefined') {
            getUser(userId).then((x) => {
                setUserData(x);
                getUserResources(x.id).then((resource) => {
                    setResources(resource)
                });
                getUserRelations(x.id).then((relation) => {
                    setNbRelation(relation.length);
                })

                getUserRelations(user.id).then((myRelation) => {
                    myRelation.forEach((e) => {
                        console.log(e.id_to)
                        console.log(x.id)
                        if (e.id_to == x.id) {
                            console.log("myFriend")
                            setIsMyFriend(true);
                            setMyRelation(e);
                        }
                    })
                })
            })

        }
    }, []);

    const RemoveFriend = () => {
        console.log("remove friend ", myRelation)
        RemoveRelation(myRelation._id).then(() => {
            window.location = window.location;
        })
    }

    const addUser = () => {
        AddRelations({
            id_from: myUser.id,
            id_to: userData.id
        }).then(() => {
            window.location = window.location;
        })
    }

    const src = userData != null ? userData.avatar : "/img/background-login.jpg";

    return (
        <Layout>
            <div className="flex flex-col mx-auto max-w-3xl">
                <div className='flex my-6 justify-between'>
                    <div className='flex'>
                        <div>
                            <div className='image-container border-solid border-2 border-purple-cube bg-white rounded-full h-24 w-24'>
                                <Image unoptimized={true} loader={() => src} src={src} layout="fill" className='rounded-full image h-24 w-24' />
                            </div>
                            {/* <Image width={45}  height={45} src="/img/background-login.jpg" className='rounded-full hover:opacity-70 cursor-pointer' /> */}
                        </div>
                        <div>
                            <div className='w-0 mb-auto mx-2 w-fit'>
                                <div className="flex">
                                    <p className='my-auto px-4 whitespace-nowrap text-xl cursor-pointer'>{userData != null ? userData.firstname + " " + userData.lastname : ''}</p>
                                </div>
                                <p className='px-4 whitespace-nowrap text-gray-500'>{userData != null ? userData.tag : ''}</p>
                                <p className='px-4 whitespace-nowrap text-gray-500'>{nbRelation} relations</p>
                            </div>
                        </div>
                    </div>

                    <div className='flex'>

                        {/* <i className="fa fa-heart-o" aria-hidden="true"></i> */}
                        {
                            !isMyFriend ?
                                <div className='flex ml-auto w-12 h-12 bg-white rounded-full text-center hover:bg-gray-200 transition cursor-pointer'>
                                    <div className='w-full ml-auto my-auto text-center text-xl text-purple-500'>
                                        <button onClick={addUser}>
                                            <i className="fa fa-user-plus" aria-hidden="true"></i>
                                        </button>

                                    </div>
                                </div>
                                :
                                <div className='flex ml-auto w-12 h-12 bg-white rounded-full text-center hover:bg-gray-200 transition cursor-pointer'>
                                    <div className='w-full ml-auto my-auto text-center text-xl text-purple-500'>
                                        <button onClick={RemoveFriend}>
                                            <PersonRemoveIcon className="mb-1"></PersonRemoveIcon>
                                        </button>
                                    </div>
                                </div>
                        }

                    </div>
                    {/* <div className='absolute top-6 flex w-12 h-12 bg-white rounded-full text-center hover:bg-gray-200 hover:rotate-360 transition cursor-pointer'>
                        <div className='w-full ml-auto my-auto text-center text-xl text-purple-500'>
                            <i className="fa fa-user-plus" aria-hidden="true"></i>
                        </div>
                    </div> */}

                </div>


            </div>
            {
                resources.map((resource) => {
                    console.log(resource);
                    return (
                        <Card key={resource._id} data={resource} />
                    )
                })
            }
        </Layout>
    )
}

// getServer
export async function getServerSideProps(req) {
    // get id in params
    // const id = req.query.id;

    // let user = null;
    // let resources = null;
    // if (id) {
    //     const responseUser = await axios.get(`http://185.171.202.52:5000/api/users/${id}`);
    //     user = responseUser.data;

    //     const responseRessources = await axios.get(`http://185.171.202.52:5000/api/resources/user?user_id=${id}`);
    //     resources = responseRessources.data;
    // }


    // let user = await getUser(userId)
    // let resource = await getUserResources(user._id); 

    return {
        props:
        {
            userId: req.query.id
        }
    }
}