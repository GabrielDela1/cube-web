import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;

function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}


async function getUserRelations (user_id) {
    let response = await axios.get(BASE_URL + "relations/friends/" + user_id, token());
    return response.data;
}

async function AddRelations (relation) {
    let response = await axios.post(BASE_URL + "relations", relation, token());
    return response.data;
}

async function RemoveRelation(idRelation) {
    let response = await axios.delete(BASE_URL + "relations/" + idRelation, token());
    return response.data;
}

export { getUserRelations, AddRelations, RemoveRelation };