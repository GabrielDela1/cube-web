import Admin from '../../../layouts/Admin';
import { useEffect, useState } from "react";
import { getComment, updateComment } from '../../../utils/admin.comment.service';


export default function Categories({ id }) {
    let [comment, setComment] = useState([]);
    let [deleted, setDeleted] = useState(false);

    useEffect(() => {
        getComment(id).then(res => {
            setComment(res);
            setDeleted(res.name);
        });
    }, []);

    const changeDeleted = (e) => {
        setDeleted(e.target.value);
    }

    return (
        <Admin>
            <button className='mr-auto px-4 py-2 bg-neutral-800 font-bol text-xs text-white mx-2 mb-4 rounded' onClick={() => history.back()}>Retour</button>

            <div className="px-2 text-sm text-left text-gray-500">
                Gestion des commentaires
            </div>

            <div className='mx-2 bg-white border-solid border-2 border-neutral-300 rounded p-4 my-4'>
                <p className='pb-6'>Informations du commentaire</p>
                <div className="relative z-0 w-full mb-6 group">
                    <textarea disabled type="text" value={comment.comment} name="floating_comment" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
                    <label htmlFor="floating_comment" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nom</label>
                </div>

                <div className="relative z-0 w-full mb-4 group">
                    <label htmlFor="deleted" className="block mb-2 text-xs font-sm text-gray-500 dark:text-gray-400">Supprimé ?</label>
                    <select id="deleted" onChange={changeDeleted} className="bg-gray-50 border border-gray-300 text-gray-600 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {
                            deleted == true ? <option value="true" selected>Oui</option> : <option value="true">Oui</option>
                        }
                        {
                            !deleted == true ? <option value="false" selected>Non</option> : <option value="false">Non</option>
                        }
                    </select>
                </div>


                <button onClick={() => {
                    let data = {
                        deleted: deleted
                    }
                    updateComment(comment._id, data);
                }} type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Éditer</button>
            </div>
        </Admin>
    )
}

export async function getServerSideProps(req) {
    return {
        props:
        {
            id: req.query.id
        }
    }
}