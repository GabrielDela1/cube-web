import Admin from '../../layouts/Admin';
import { useEffect, useState } from "react";
import ResourceLine from '../../components/ResourceLine';
import { getResources } from '../../utils/admin.resource.service';
import Link from "next/link";


export default function Resources() {
    let [resources, setResources] = useState([]);

    useEffect(() => {
        getResources().then(res => {
            setResources(res);
        });
    }, []);

    return (
        <Admin>
            <div className="flex px-4 text-sm text-left text-gray-500">
                <p className='my-auto'>Gestion des ressources</p>
            </div>

            <table className="mx-2 text-sm px-4 text-left text-gray-500 my-4 ">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Titre
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Statut
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Utilisateur
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Date de création
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        resources.map((resource) => {
                            return (
                                <ResourceLine key={resource._id} data={resource} />
                            )
                        })
                    }
                </tbody>
            </table>
        </Admin>
    )
}