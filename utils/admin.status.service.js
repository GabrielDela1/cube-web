import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;
let prefix = 'admin/status/';

// get header token
function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}

async function getStatuses() {
    let res = await axios.get(BASE_URL + prefix, token());
    return res.data;
}

// get user admin
async function getStatus(_id) {
    let response = await axios.get(BASE_URL + prefix + _id, token());
    return response.data;
}

// delete user admin
async function removeStatus(_id) {
    // promt user to confirm
    if (window.confirm("Voulez-vous vraiment supprimer ce statut ?")) {
        let response = await axios.delete(BASE_URL + prefix + _id, token());

        if (response.status === 200) {
            window.location.href = '/admin/status';
        }
        else {
            alert("Erreur lors de la suppression du statut");
        }
        return response.data;
    }
}

async function createStatus(name) {
    let response = await axios.post(BASE_URL + prefix, { name: name }, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/status';
    }
    else {
        alert("Erreur lors de la création du statut");
    }

    return response.data;
}

async function updateStatus(_id, name) {
    let response = await axios.patch(BASE_URL + prefix + _id, { name: name }, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/status';
    }
    else {
        alert("Erreur lors de la modification du statut");
    }

    return response.data;
}

export { getStatuses, getStatus, removeStatus, createStatus, updateStatus };