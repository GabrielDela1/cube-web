import Link from "next/link";
import { removeStatus } from "../utils/admin.status.service.js";
import URL from '../utils/url.js';
const BASE_URL = URL;

export default function StatusLine({ data }) {


    return (
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
            <td className="px-6 py-4">
                {data.name}
            </td>
            <td className="flex flex-col px-6 py-4 text-right">
                <Link href={`/admin/status/${data._id}`}>
                    <button className="cursor-pointer font-medium text-blue-600 dark:text-blue-500 hover:underline">Éditer</button>
                </Link>
                <button onClick={() => removeStatus(data._id)} className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Supprimer</button>
            </td>
        </tr>
    )
}