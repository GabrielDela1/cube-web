import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;

function token() {
  return {
      headers: {
          Authorization: "Bearer " + window.localStorage.getItem('token'),
      },
  };
}

// get user resources
async function getAllCategories() {
  let response = await axios.get(BASE_URL + "categories/", token());
  return response.data;
}

export { getAllCategories };