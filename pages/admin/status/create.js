import Admin from '../../../layouts/Admin';
import { useEffect, useState } from "react";
import { createStatus } from '../../../utils/admin.status.service';


export default function Status() {
    let [name, setName] = useState("");

    const changeName = (e) => {
        setName(e.target.value);
    }

    return (
        <Admin>
            <button className='mr-auto px-4 py-2 bg-neutral-800 font-bol text-xs text-white mx-2 mb-4 rounded' onClick={() => history.back()}>Retour</button>
            
            <div className="px-2 text-sm text-left text-gray-500">
                Gestion des statuts
            </div>

            <div className='mx-2 bg-white border-solid border-2 border-neutral-300 rounded p-4 my-4'>
                <p className='pb-6'>Informations du statut</p>
                <div className="relative z-0 w-full mb-6 group">
                    <input type="string" value={name} onChange={changeName} name="floating_name" className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
                    <label htmlFor="floating_name" className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nom</label>
                </div>
                
                <button onClick={() => {createStatus(name)}} type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Créer</button>
            </div>
        </Admin>
    )
}