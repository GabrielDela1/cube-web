import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;
let prefix = 'admin/categories/';

// get header token
function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}

async function getCategories() {
    let res = await axios.get(BASE_URL + prefix, token());
    return res.data;
}

// get user admin
async function getCategory(_id) {
    let response = await axios.get(BASE_URL + prefix + _id, token());
    return response.data;
}

// delete user admin
async function removeCategory(_id) {
    // promt user to confirm
    if (window.confirm("Voulez-vous vraiment supprimer la catégorie ?")) {
        let response = await axios.delete(BASE_URL + prefix + _id, token());

        if (response.status === 200) {
            window.location.href = '/admin/categories';
        }
        else {
            alert("Erreur lors de la suppression de la catégorie");
        }
        return response.data;
    }
}

async function createCategory(name) {
    let response = await axios.post(BASE_URL + prefix, { name: name }, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/categories';
    }
    else {
        alert("Erreur lors de la création de la catégorie");
    }

    return response.data;
}

async function updateCategory(_id, name) {
    let response = await axios.patch(BASE_URL + prefix + _id, { name: name }, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/categories';
    }
    else {
        alert("Erreur lors de la modification de la catégorie");
    }

    return response.data;
}

export { getCategories, getCategory, removeCategory, createCategory, updateCategory };