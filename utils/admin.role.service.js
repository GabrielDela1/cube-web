import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;
let prefix = 'admin/roles/';

// get header token
function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}

async function getRoles() {
    let res = await axios.get(BASE_URL + prefix, token());
    return res.data;
}

// get roles admin
async function getRole(_id) {
    let response = await axios.get(BASE_URL + prefix + _id, token());
    return response.data;
}

// delete roles admin
async function removeRole(_id) {
    // promt roles to confirm
    if (window.confirm("Voulez-vous vraiment supprimer ce rôle ?")) {
        let response = await axios.delete(BASE_URL + prefix + _id, token());

        if (response.status === 200) {
            window.location.href = '/admin/roles';
        }
        else {
            alert("Erreur lors de la suppression du rôle");
        }
        return response.data;
    }
}

async function createRole(name) {
    let response = await axios.post(BASE_URL + prefix, { name: name }, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/roles';
    }
    else {
        alert("Erreur lors de la création du rôle");
    }

    return response.data;
}

async function updateRole(_id, name) {
    let response = await axios.patch(BASE_URL + prefix + _id, { name: name }, token());
    
    if (response.status === 200) {
        window.location.href = '/admin/roles';
    }
    else {
        alert("Erreur lors de la modification du rôle");
    }

    return response.data;
}

export { getRoles, getRole, removeRole, createRole, updateRole };