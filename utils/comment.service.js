import Router from "next/router";
import axios from "axios";
import URL from "./url.js"

const BASE_URL = URL;
let prefix = 'comments/';

// get header token
function token() {
    return {
        headers: {
            Authorization: "Bearer " + window.localStorage.getItem('token'),
        },
    };
}

async function getComments(_id){
  let response = await axios.get(BASE_URL + prefix + "latest/" + _id, token());
  return response.data;
}

async function createComment(_id, data){
  let response = await axios.post(BASE_URL + prefix, data, token());

  window.location = window.location;

  return response.data;
}

export { getComments, createComment };