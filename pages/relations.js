
import Image from 'next/image'
import Layout from '../layouts/Layout'
import Relation from '../components/Relation';
import { useEffect, useState } from "react";
import { me } from '../utils/auth.service';
import { getUserRelations } from '../utils/relation.service'
import { getAllUsers } from '../utils/user.service';
import PersonSearchIcon from '@mui/icons-material/PersonSearch';

export default function Relations() {
  // let [searchUsers, setSearchUsers] = useState([]);
  // let [newFriendList, setNewFriendList] = useState([]);
  // let [users, setUsers] = useState([]);
  let [searchValue, setSearchValue] = useState("");
  let [users, setUsers] = useState([]);
  let [filteredUsers, setFilteredUsers] = useState([]);
  let [relations, setRelations] = useState([]);

  useEffect(async () => {


    me().then(me => {
      getAllUsers().then(res => {
        let temp = res.find(e => e.id == me.data.user.user._id);
        let index = res.indexOf(temp);
        res.splice(index, 1);
        setUsers(res);
      });


      getUserRelations(me.data.user.user._id).then((x) => {
        // console.log(x)
        // let temp = [];
        // x.forEach(relation => {
        //   temp.push(relation.user);
        // });

        setRelations(x);
      })
    });
  }, []);

  const ChangeSetSearchValue = (e) => {
    if (e.target.value.length > 0) {

      let temp = [];
      users.forEach(x => {
        if (x.firstname.includes(e.target.value) || x.lastname.includes(e.target.value) || x.tag.includes(e.target.value)) {
          temp.push(x);
        }
      });

      setFilteredUsers(temp);
    }

    setSearchValue(e.target.value);
  }

  return (
    <Layout>
      <div className='flex flex-col mx-auto max-w-3xl'>
        <div className='flex justify-between p-5'>
          <div className='text-xl font-semibold tracking-widest my-auto'>
            {"Liste des relations"}
          </div>
          <Image src="/img/logo.png" width='50px' height='50px'></Image>
        </div>

        <label className="relative block  mb-5" >
          <span className="sr-only">Search</span>
          <span className="absolute inset-y-0 left-0 flex items-center pl-2">
            <svg className="h-5 w-5 fill-slate-300" viewBox="0 0 20 20"><PersonSearchIcon></PersonSearchIcon></svg>
          </span>
          <input onChange={ChangeSetSearchValue} className="placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm" placeholder="Search for anything..." type="text" name="search" />
        </label>
        {
          searchValue != "" ?
            filteredUsers.map((user, index) => {
              let isFriend = false;
              console.log("relation ", relations)
              console.log("filteredUser", filteredUsers)




              if (relations.forEach(userInRelation => {
                console.log("userInRelation ", userInRelation);
                console.log("user", user)
                if (userInRelation.user.tag == user.tag) {
                  user = userInRelation;
                  isFriend = true;
                  console.log(isFriend)
                }
              }
              ))
                console.log(user, isFriend)
              return (
                <Relation key={index} dataUser={user} isFriend={isFriend} />
              )
            })
            :
            relations.map((relation, index) => {
              console.log(relation)
              return (
                <Relation key={index} dataUser={relation} isFriend={true} />
              )
            })
        }
      </div>
    </Layout>
  )
}
