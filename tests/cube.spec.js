describe('The utils folders services', () => {
  describe('Resources service', () => {

    // get data of Promise
    it('should return a list of resources', async () => {
      let resources = await getResources();
      expect(resources).toHaveLength(3);
    });

    it('should be an object list', async () => {
      const resources = await getResources();
      expect(resources).toBeInstanceOf(Array);
    });

    it('must contain objects', async () => {
      const resources = await getResources();
      expect(resources[0]).toBeInstanceOf(Object);
    });

    // must be a valid resource model
    it('should be a valid resource model', async () => {
      const resources = await getResources();
      expect(resources[0]._id).toBeDefined();
      expect(resources[0].title).toBeDefined();
      expect(resources[0].description).toBeDefined();
      expect(resources[0].user_id).toBeDefined();
      expect(resources[0].category_id).toBeDefined();
      expect(resources[0].type_id).toBeDefined();
      expect(resources[0].status).toBeDefined();
    });
  });

  describe('Users service', () => {
    // get data of Promise
    it('should return a list of users', async () => {
      let users = await getUsers();
      expect(users).toHaveLength(2);
    });

    it('should be an object list', async () => {
      const users = await getUsers();
      expect(users).toBeInstanceOf(Array);
    });

    it('must contain objects', async () => {
      const users = await getUsers();
      expect(users[0]).toBeInstanceOf(Object);
    });

    // must be a valid resource model
    it('should be a valid users model', async () => {
      const users = await getUsers();
      expect(users[0]._id).toBeDefined();
      expect(users[0].firstname).toBeDefined();
      expect(users[0].lastname).toBeDefined();
      expect(users[0].tag).toBeDefined();
      expect(users[0].email).toBeDefined();
      expect(users[0].age).toBeDefined();
      expect(users[0].avatar).toBeDefined();
    });
  });
});

async function getUsers() {
  return [
    {
      _id: "168813153",
      firstname: "John",
      lastname: "Doe",
      tag :  "@JoDo123456",
      biography: "This is a biography",
      email: "johndoe@xyz.com",
      password: "123456",
      google_id: "",
      avatar: "https://via.placeholder.com/150",
      age : 0,
      role : "user",
      status: "active",
      favorites: [],
    },
    {
      _id: "vnzourznvzeej",
      firstname: "John",
      lastname: "Doe",
      tag :  "@JoDo123456",
      biography: "This is a biography",
      email: "johndoe@xyz.com",
      password: "123456",
      google_id: "",
      avatar: "https://via.placeholder.com/150",
      age : 0,
      role : "user",
      status: "active",
      favorites: [],
    }
  ];
}

async function getResources() {
  return [
    {
      _id: '1',
      title: "Resource 1",
      description: "This is a resource",
      image: "https://via.placeholder.com/150",
      content: "This is the content of the resource",
      user_id: "1",
      likes: "0",
      share: "0",
      category_id: "1",
      type_id: "1",
      created_at: "2020-01-01",
      updated_at: "2020-01-01",
      status: "active",
    },
    {
      _id: '2',
      title: "Resource 2",
      description: "This is a resource",
      image: "https://via.placeholder.com/150",
      content: "This is the content of the resource",
      user_id: "1",
      likes: "0",
      share: "0",
      category_id: "1",
      type_id: "1",
      created_at: "2020-01-01",
      updated_at: "2020-01-01",
      status: "active",
    },
    {
      _id: '3',
      title: "Resource 3",
      description: "This is a resource",
      image: "https://via.placeholder.com/150",
      content: "This is the content of the resource",
      user_id: "1",
      likes: "0",
      share: "0",
      category_id: "1",
      type_id: "1",
      created_at: "2020-01-01",
      updated_at: "2020-01-01",
      status: "active",
    }
  ];
}