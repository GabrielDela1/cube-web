import Admin from '../../layouts/Admin';
import { useEffect, useState } from "react";
import UserLine from '../../components/UserLine';
import { getUsers } from '../../utils/admin.user.service';
import Link from "next/link";


export default function Users() {
    let [users, setUsers] = useState([]);

    useEffect(() => {
        getUsers().then(res => {
            setUsers(res);
        });
    }, []);

    return (
        <Admin>
            <div className="flex px-4 text-sm text-left text-gray-500">
                <p className='my-auto'>Gestion des utilisateurs</p>
                <Link href={`/admin/users/create`}>
                    <button className='ml-auto bg-neutral-800 text-white font-bold text-xs px-4 py-2 rounded'>Ajouter</button>
                </Link>
            </div>

            <table className="mx-2 text-sm px-4 text-left text-gray-500 my-4 ">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Avatar
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Tag
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Nom - Prénom
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Rôle
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Statut
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        users.map((user) => {
                            return (
                                <UserLine key={user._id} data={user} />
                            )
                        })
                    }
                </tbody>
            </table>
        </Admin>
    )
}