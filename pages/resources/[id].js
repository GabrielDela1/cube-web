import Image from 'next/image';
import Card from '../../components/Card';
import Layout from '../../layouts/Layout';
import Link from 'next/link';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { me } from '../../utils/auth.service';
import { getResource } from '../../utils/resource.service';
import { getUser } from '../../utils/user.service';
import { getComments } from '../../utils/comment.service';
import { createComment } from '../../utils/comment.service';
import Comment from '../../components/Comment';


// URL from util
import URL from '../../utils/url.js';
const BASE_URL = URL;

export default function Resource({ id }) {
    let [resource, setResources] = useState({});
    let [user, setUser] = useState({});
    let [formatedDate, setFormatedDate] = useState("");
    let [comments, setComments] = useState([]);
    let [comment, setComment] = useState("");
    let [self, setSelf] = useState("");

    useEffect(async () => {
        let response = await me();
        setSelf(response.data.user.user);
        getResource(id).then((resource) => {
            let date = resource != null ? new Date(resource.created_at) : new Date();
            var formatedDay = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            var formatedMonth = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
            setFormatedDate(formatedDay + "/" + formatedMonth + "/" + date.getFullYear());
            setResources(resource);

            createElementFromHTML(resource.content);
            getUser(resource.user_id).then((user) => {
                setUser(user);
            });

            getComments(resource._id).then((comments) => {
                setComments(comments);
                console.log("comments", comments);
            });
        });
    }, []);

    const changeComment = (e) => {
        setComment(e.target.value);
    }

    const src = resource != null ? BASE_URL + 'images/' + resource.image : null;

    return (
        <Layout>
            <div className='w-full'>
                <div className='flex flex-col w-full sm:max-w-xl lg:max-w-3xl mx-auto rounded-lg'>
                    <div className='image-container relative w-full h-72'>
                        {resource.image ? <Image loader={() => src} src={src} alt={resource.image} layout="fill" className='rounded-b-lg object-cover image w-full h-72 object-cover' /> : null}
                    </div>
                    <div className='bg-white mt-4 rounded-lg mb-2'>
                        <h1 className='my-4 mx-6 text-center text-3xl'>{resource.title}</h1>
                        <h4 className='my-2 mx-10 text-center text-1xl text-gray-800'>{resource.description}</h4>
                        <p className='my-10 px-4' id="content">
                        </p>
                        <div className="flex justify-between mb-10">
                            <p className='ml-16'>{user.firstname + " " + user.lastname}</p>
                            <p className='mr-16'>{formatedDate}</p>
                        </div>
                    </div>

                    <textarea value={comment} onChange={changeComment} className='w-full h-32 mt-4 p-2' placeholder='Ajouter un commentaire'></textarea>
                    <button onClick={() => {
                        let data = {
                            comment: comment,
                            id_resource: resource._id,
                            id_user: self._id,
                        }
                        createComment(resource._id, data);
                    }} className='w-full my-4 bg-blue-600 text-white font-bold py-2 px-4 rounded-lg'>Ajouter</button>

                    <div className='flex flex-col w-full mt-4'>
                        <p className='py-2'>Commentaires</p>
                        {comments.map((comment) => {
                            return <Comment key={comment._id} data={comment}/>
                        })
                        }
                    </div>
                </div>
            </div>

        </Layout>
    )
}

export async function getServerSideProps(req) {
    var id = req.params.id;

    return {
        props: {
            id: req.params.id
        }
    };

    // var resources = [];

    // var responseRessource = await axios.get('http://185.171.202.52:5000/api/resources/' + id,{
    //     headers: {
    //       Authorization: "Bearer " + window.localStorage.getItem('token'),
    //     },
    //   });
    // var responseUser = await axios.get('http://185.171.202.52:5000/api/users/' + responseRessource.data.user_id,{
    //     headers: {
    //       Authorization: "Bearer " + window.localStorage.getItem('token'),
    //     },
    //   });

    // return {
    //     props: {
    //         resource: responseRessource.data,
    //         user: responseUser.data,
    //     }
    // };
}

function createElementFromHTML(htmlString) {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();
    document.getElementById('content').appendChild(div);
    // return div.firstChild;
}