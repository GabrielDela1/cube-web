
import Image from 'next/image';
import Card from '../components/Card';
import Layout from '../layouts/Layout';
import { me } from '../utils/auth.service.js';
import { useEffect, useState } from "react";
import { getAllResources } from '../utils/resource.service';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { getAllCategories } from "../utils/categorie.service";
import { getAllTypes } from "../utils/type.service";

export default function Home() {
  let [user, setUser] = useState(null);
  let [resources, setResources] = useState([]);
  let [filteredResources, setFilteredResources] = useState([]);

  let [categories, setCategories] = useState([]); // props.categories
  let [category, setCategory] = useState("");
  let [type, setType] = useState("");
  let [types, setTypes] = useState([]); // props.types

  const changeCategory = (e) => {
    console.log("changeCategory");
    setCategory(e.target.value);
    filterResources(type, e.target.value);
  }

  const changeType = (e) => {
    console.log("changeType");
    setType(e.target.value);
    filterResources(e.target.value, category);
  }

  const filterResources = (p_type, p_category) => {
    if(p_type === "" && p_category === "") {
      setFilteredResources(resources);
    }
    else if(p_type === "") {
      setFilteredResources(resources.filter(x => x.category_id === p_category));
    } 
    else if(p_category === "") {
      setFilteredResources(resources.filter(x => x.type_id === p_type));
    }else {
      setFilteredResources(resources.filter(x => x.category_id === p_category && x.type_id === p_type));
    }
  }

  useEffect(async () => {
    me();

    getAllResources().then(res => {
      setFilteredResources(res.data);
      setResources(res.data);
    });

    let user = null;
    if (typeof window !== 'undefined') {
      user = JSON.parse(window.localStorage.getItem("user"));
    }

    setUser(user);

    getAllCategories().then((x) => {
      setCategories(x);
    })

    getAllTypes().then((x) => {
      setTypes(x);
    })
  }, []);

  return (
    <Layout >
      <div className='flex flex-col mx-auto max-w-3xl'>
        <div className='flex justify-between px-5 pt-5 '>
          <div className='text-xl font-semibold tracking-widest my-auto'>
            Accueil
          </div>
          <Image src="/img/logo.png" width='50px' height='50px'></Image>
        </div>
        <div className='relative w-full mt-6'>
          <select onChange={changeCategory} id="category_id" className='hover:bg-gray-200 transition w-full outline-none focus:outline-none text-xl px-6 py-2 shadow-xl rounded-full appearance-none'>
            <option value="" key="categories" selected>Catégorie</option>
            {
              categories.map(e => {
                return <option value={e._id} key={e._id}>{e.name}</option>
              })
            }
          </select>
          <div className="relative ml-auto w-fit text-right -top-9 -left-4"><KeyboardArrowDownIcon></KeyboardArrowDownIcon></div>
        </div>

        <div className='relative w-full'>
          <select onChange={changeType} id="type_id" className='hover:bg-gray-200 transition w-full outline-none focus:outline-none text-xl px-6 py-2 shadow-xl rounded-full appearance-none'>
            <option value="" key="types" selected>Type</option>
            {
              types.map(e => {
                return <option value={e._id} key={e._id}>{e.name}</option>
              })
            }
          </select>
          <div className="relative ml-auto w-fit text-right -top-9 -left-4"><KeyboardArrowDownIcon></KeyboardArrowDownIcon></div>
        </div>

        {
          filteredResources ?
          filteredResources.map((resource) => {
              return <Card key={resource._id} data={resource}></Card>
            })
            :
            null
        }
      </div>

    </Layout >
  )
}
