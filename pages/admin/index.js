import Admin from '../../layouts/Admin';
import { useEffect, useState } from "react";

export default function Users() {
    useEffect(() => {
        window.location.href = '/admin/users';
    }, []);

    return (
        <Admin>
        </Admin>
    )
}