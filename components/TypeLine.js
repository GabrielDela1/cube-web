import Link from "next/link";
import Image from 'next/image';
import { removeType } from "../utils/admin.type.service.js";
import URL from '../utils/url.js';
const BASE_URL = URL;

export default function TypeLine({ data }) {


    return (
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
            <td className="px-6 py-4">
                {data.name}
            </td>
            <td className="flex flex-col px-6 py-4 text-right">
                <Link href={`/admin/types/${data._id}`}>
                    <button className="cursor-pointer font-medium text-blue-600 dark:text-blue-500 hover:underline">Éditer</button>
                </Link>
                <button onClick={() => removeType(data._id)} className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Supprimer</button>
            </td>
        </tr>
    )
}